FROM ubuntu

ENV DEBIAN_FRONTEND=noninteractive

RUN set -x \
    && apt-get update \
    && apt-get install -y -qq --no-install-recommends \
         build-essential \
         ca-certificates \
         cmake \
         curl \
         exiftran \
         imagemagick \
         jpegoptim \
         libcpanel-json-xs-perl \
         libimage-exiftool-perl \
         libjpeg-dev \
         liblcms2-utils \
         libpng-dev \
         libtiff-dev \
         opencv-data \
         p7zip \
         perl \
         pngcrush \
         python3-dev \
         python3-numpy \
         unzip \
         zip \
         git

RUN git clone https://github.com/kensanata/sitelen-mute.git /usr/share/sitelen-mute \
    && ln -s /usr/share/sitelen-mute/sitelen-mute /usr/bin

WORKDIR /home
COPY src/create_galleries /usr/bin/
ENTRYPOINT ["sitelen-mute"]
CMD ["--viewdir", "/usr/share/sitelen-mute/view", "-f"]
